package com.yusuf.resource;

import com.yusuf.model.User;
import com.yusuf.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Y.A on 27-1-2017.
 */
@RestController
@RequestMapping("/users")
public class UserResource {
    @Autowired
    private UserRepository userRepository;

    @RequestMapping(method = RequestMethod.GET)
    public List<User> getAllUsers() {
        return (List<User>) userRepository.findAll();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public User getUserWithId(@PathVariable Long id) {
        return userRepository.findOne(id);
    }
}

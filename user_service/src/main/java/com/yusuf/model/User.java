package com.yusuf.model;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Created by Y.A on 27-1-2017.
 */

@Entity
@Data
@Builder
public class User {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private long id;
    @NotNull
    @Column(unique = true)
    private String username;
    @NotNull
    @Min(value = 6)
    private String password;

    @Tolerate
    public User() {
    }
}

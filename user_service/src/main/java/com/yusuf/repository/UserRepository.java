package com.yusuf.repository;

import com.yusuf.model.User;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Y.A on 27-1-2017.
 */
public interface UserRepository extends CrudRepository<User, Long> {
}
